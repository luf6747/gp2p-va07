import studiplayer.basic.BasicPlayer;

public abstract class SampledFile extends AudioFile {

    protected String duration = "";

    SampledFile() {
        super();
    }

    SampledFile(String pathname) {
        super(pathname);
    }

    public void play() {
        BasicPlayer.play(super.getPathname());
    }

    public void togglePause() {
        BasicPlayer.togglePause();
    }

    public void stop() {
        BasicPlayer.stop();
    }

    public String getFormattedDuration() { // us = microseconds
        long us = Long.parseLong(this.duration);
        return timeFormatter(us);
    }

    public String getFormattedPosition() {
        long us = studiplayer.basic.BasicPlayer.getPosition();
        return timeFormatter(us);
    }

    public static String timeFormatter(long microtime) {
        if (microtime < 0)
            throw new RuntimeException("Negative time value for microtime received!");
        long secs = microtime / 1000000;
        long mins = secs / 60;
        secs = secs % 60;
        if (mins >= 100)
            throw new RuntimeException("microtime >= 60min received!");
        return String.format("%02d:%02d", mins, secs);
    }
}
