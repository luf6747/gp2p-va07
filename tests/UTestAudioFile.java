import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class UTestAudioFile {
    private boolean isWindows(){
        return System.getProperty("os.name").toLowerCase().indexOf("win") >= 0;
    }
    @Test
    public void test_parsePathname_00() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("");
        // On Unix      we expect "/adad/2eafd/d.mp3"
        // On Windows   we expect "\adad\2eafd\d.mp3"
        assertEquals("Pathname stored incorrectly", 
                "",
                af.getPathname());
        assertEquals("", 
                "",
                af.getFilename());
    }
    @Test
    public void test_parsePathname_01() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("   \t");
        // On Unix      we expect "/adad/2eafd/d.mp3"
        // On Windows   we expect "\adad\2eafd\d.mp3"
        assertEquals("Pathname stored incorrectly", 
                "   \t",
                af.getPathname());
        assertEquals("Return filename incorrect", 
                "   \t",
                af.getFilename());
    }
    @Test
    public void test_parsePathname_02() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("file.mp3");
        // On Unix      we expect "/adad/2eafd/d.mp3"
        // On Windows   we expect "\adad\2eafd\d.mp3"
        assertEquals("Pathname stored incorrectly", 
                "file.mp3",
                af.getPathname());
        assertEquals("Return filename incorrect", 
                "file.mp3",
                af.getFilename());
    }
    @Test
    public void test_parsePathname_03() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("/my-tmp/file.mp3");
        char sepchar = java.io.File.separatorChar;
        // On Unix      we expect "/adad/2eafd/d.mp3"
        // On Windows   we expect "\adad\2eafd\d.mp3"
        assertEquals("Pathname stored incorrectly", 
                sepchar + "my-tmp" + sepchar + "file.mp3",
                af.getPathname());
        assertEquals("Return filename incorrect", 
                "file.mp3",
                af.getFilename());
    }
    @Test
    public void test_parsePathname_04() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("//my-tmp////part1//file.mp3/");
        char sepchar = java.io.File.separatorChar;
        // On Unix      we expect "/adad/2eafd/d.mp3"
        // On Windows   we expect "\adad\2eafd\d.mp3"
        assertEquals("Pathname stored incorrectly", 
                sepchar + "my-tmp" + sepchar + "part1" + sepchar + "file.mp3" + sepchar,
                af.getPathname());
        assertEquals("Return filename incorrect", 
                "",
                af.getFilename());
    }
    @Test
    public void test_parsePathname_05() throws Exception {
        AudioFile af = new AudioFile();
        af.parsePathname("d:\\\\part1///file.mp3");
        char sepchar = java.io.File.separatorChar;
        String Prefix = "";
        if(isWindows()) Prefix = "d:";
        else Prefix = sepchar + "d";
        // On Unix      we expect "/d/part1/file.mp3"
        // On Windows   we expect "d:\part1\file.mp3"
        assertEquals("Pathname stored incorrectly", 
                Prefix + sepchar + "part1" + sepchar+ "file.mp3",
                af.getPathname());
        assertEquals("Return filename incorrect", 
                "file.mp3",
                af.getFilename());
    }
    @Test
    public void test_parseFilename_35() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname(" Falco  -  Rock me    Amadeus .mp3");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                " Falco  -  Rock me    Amadeus .mp3",
                af.getFilename());

        assertEquals("Author stored incorrectly","Falco",af.getAuthor());
        assertEquals("Title stored incorrectly","Rock me    Amadeus",af.getTitle());
    }
    @Test
    public void test_parseFilename_36() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("Frankie Goes To Hollywood - The Power Of Love.ogg");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "Frankie Goes To Hollywood - The Power Of Love.ogg",
                af.getFilename());

        assertEquals("Author stored incorrectly","Frankie Goes To Hollywood",af.getAuthor());
        assertEquals("Title stored incorrectly","The Power Of Love",af.getTitle());
    }
    @Test
    public void test_parseFilename_37() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("audiofile.aux");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "audiofile.aux",
                af.getFilename());

        assertEquals("Author stored incorrectly","",af.getAuthor());
        assertEquals("Title stored incorrectly","audiofile",af.getTitle());
    }
    @Test
    public void test_parseFilename_38() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("/tmp/test/    A.U.T.O.R     -   T.I.T.E.L     .EXTENSION");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "    A.U.T.O.R     -   T.I.T.E.L     .EXTENSION",
                af.getFilename());

        assertEquals("Author stored incorrectly","A.U.T.O.R",af.getAuthor());
        assertEquals("Title stored incorrectly","T.I.T.E.L",af.getTitle());
    }
    @Test
    public void test_parseFilename_39() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("Hans-Georg Sonstwas - Blue-eyed boy-friend.mp3");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "Hans-Georg Sonstwas - Blue-eyed boy-friend.mp3",
                af.getFilename());

        assertEquals("Author stored incorrectly","Hans-Georg Sonstwas",af.getAuthor());
        assertEquals("Title stored incorrectly","Blue-eyed boy-friend",af.getTitle());
    }
    @Test
    public void test_parseFilename_40() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname(".mp3");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                ".mp3",
                af.getFilename());

        assertEquals("Author stored incorrectly","",af.getAuthor());
        assertEquals("Title stored incorrectly","",af.getTitle());
    }
    @Test
    public void test_parseFilename_41() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("Falco - Rock me Amadeus.");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "Falco - Rock me Amadeus.",
                af.getFilename());

        assertEquals("Author stored incorrectly","Falco",af.getAuthor());
        assertEquals("Title stored incorrectly","Rock me Amadeus",af.getTitle());
    }
    @Test
    public void test_parseFilename_42() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname("-");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                "-",
                af.getFilename());

        assertEquals("Author stored incorrectly","",af.getAuthor());
        assertEquals("Title stored incorrectly","-",af.getTitle());
    }
    @Test
    public void test_parseFilename_43() throws Exception {
        AudioFile af = new AudioFile();
        
        af.parsePathname(" - ");
        af.parseFilename(af.getFilename());
        
        assertEquals("Filename stored incorrectly",
                " - ",
                af.getFilename());

        assertEquals("Author stored incorrectly","",af.getAuthor());
        assertEquals("Title stored incorrectly","",af.getTitle());
    }
    
    @Test
    public void test_toString_50() throws Exception {
        AudioFile af = new AudioFile("///linux/haxx/1 - so und .wav");
        assertEquals("Author wrong:","1",af.getAuthor());
        assertEquals("Title wrong:","so und",af.getTitle());
        assertEquals("toSring() failed:","1 - so und","" + af);
    }
    @Test
    public void test_toString_51() throws Exception {
        AudioFile af = new AudioFile("schneaky - ");
        assertEquals("toSring() failed:","schneaky - ","" + af);
    }
    @Test
    public void test_toString_52() throws Exception {
        AudioFile af = new AudioFile("schneaky -     2a    ");
        assertEquals("toSring() failed:","schneaky - 2a","" + af);
    }
    @Test
    public void test_toString_53() throws Exception {
        AudioFile af = new AudioFile(" - ");
        assertEquals("toSring() failed:","","" + af);
    }
    @Test
    public void test_toString_54() throws Exception {
        AudioFile af = new AudioFile("- ");
        assertEquals("toSring() failed:","-","" + af);
    }
    @Test
    public void test_toString_55() throws Exception {
        AudioFile af = new AudioFile(" -  alsa Audio");
        assertEquals("toSring() failed:","alsa Audio","" + af);
    }
    @Test
    public void test_toString_56() throws Exception {
        AudioFile af = new AudioFile("BMW  -  alsa Audi .musik");
        assertEquals("toSring() failed:","BMW - alsa Audi","" + af);
    }
    @Test
    public void test_toString_57() throws Exception {
        AudioFile af = new AudioFile("georg  -   -   - 1111   .manpage");
        assertEquals("toSring() failed:","georg - -   - 1111","" + af.toString());
    }
}
